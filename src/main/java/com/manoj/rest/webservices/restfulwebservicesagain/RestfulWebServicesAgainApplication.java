package com.manoj.rest.webservices.restfulwebservicesagain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestfulWebServicesAgainApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestfulWebServicesAgainApplication.class, args);
	}

}
